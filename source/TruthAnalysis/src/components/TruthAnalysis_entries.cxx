
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../TruthAnalysisAlg.h"

DECLARE_ALGORITHM_FACTORY( TruthAnalysisAlg )

DECLARE_FACTORY_ENTRIES( TbTruthAnalysis ) 
{
  DECLARE_ALGORITHM( TruthAnalysisAlg );
}
