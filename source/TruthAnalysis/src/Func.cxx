//Definition of the class Func to calculate general stuff
#include "Func.h"

Func::Func(){}
Func::~Func(){}

double Func::Angle(TLorentzVector v1, TLorentzVector v2)
{
  double theta = TMath::ACos( (v1.Vect()*v2.Vect()) / (v1.Vect().Mag()*v2.Vect().Mag()));

  return theta;
}

double Func::DistEtaPhi(double eta1, double phi1, double eta2, double phi2)
{
  double delta_eta = TMath::Abs(eta1-eta2);
  double delta_phi = TMath::Abs(Func::DeltaPhi(phi1,phi2)); 

  double r2 = (delta_eta*delta_eta)+(delta_phi*delta_phi);
  double r = TMath::Sqrt(r2);

  return r;
}

double Func::DeltaPhi(double phi1,double phi2)
{
  double pi = TMath::Pi();
  double delta_phi=-1000.;
  if(phi1 >= 0.0 && phi2 >= 0.0){
    delta_phi=phi1-phi2;
  }else if(phi1 < 0.0 && phi2 < 0.0){
    delta_phi=phi1-phi2;
  }else if(phi1 >= 0.0 && phi2 < 0.0){
    delta_phi = phi1-phi2;
    if(delta_phi > pi){delta_phi=(phi1-pi)+(-pi-phi2);}
  }else if(phi1 < 0.0 && phi2 >= 0.0){
    delta_phi=phi1-phi2;
    if(delta_phi < -pi){delta_phi=(phi1+pi)+(pi-phi2);}
  }

  return delta_phi;
}

int Func::findMuspContainer(std::vector<float> *musp_eta, std::vector<float> *musp_phi,float jet_eta, float jet_phi) 
{
  float delR = 0;
  float delta_phi;
  float delta_eta;

  // Match mspn container to jet delR<0.4
  delR = 100;
  int index_musp=-1;
  for (unsigned int i = 0; i < musp_phi->size(); i++) {
    delta_phi = fabs(jet_phi - musp_phi->at(i)); //calculate the distance in phi
    if (delta_phi > TMath::Pi()) delta_phi = (2 * TMath::Pi()) - delta_phi; // always take the smaller angle (below 180°)
    delta_eta = jet_eta - musp_eta->at(i); // distance in eta
    if (sqrt( pow(delta_phi,2) + pow(delta_eta,2)) < delR) {
      delR = sqrt( pow(delta_phi,2) + pow(delta_eta,2));
      index_musp = i;
    }
  }
  if (delR < 0.4){
    return index_musp;
  }
  return -1;
}

int Func::samplingNumber(std::string detName){
  if(detName =="PreSamplerB"){
    return 0;
  }else if(detName == "EMB1"){
    return 1;
  }else if(detName == "EMB2"){
    return 2;
  }else if(detName == "EMB3"){
    return 3;
  }else if(detName =="PreSamplerE"){
    return 4;
  }else if(detName == "EME1"){
    return 5;
  }else if(detName == "EME2"){
    return 6;
  }else if(detName == "EME3"){
    return 7;
  }else{
    return -1;
  }
}
