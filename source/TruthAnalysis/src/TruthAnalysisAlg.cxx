// TbTruthAnalysis includes
#include "TruthAnalysisAlg.h"
#include "Func.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODEventInfo/EventInfo.h"
#include "TLorentzVector.h"
#include "cmath"

bool ptOrdering(const TLorentzVector& first, const TLorentzVector& second){
  if(first.Pt() > second.Pt()) return true;
  return false;
}


TruthAnalysisAlg::TruthAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator )
 ,m_truthWeightTool ("PMGTools::PMGTruthWeightTool",this){
   
   declareProperty("weightName",m_weightName);
}


TruthAnalysisAlg::~TruthAnalysisAlg() {}


StatusCode TruthAnalysisAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
  CHECK( histSvc.retrieve() );
  CHECK(m_truthWeightTool.retrieve());

  sumOfWeights=0;

  //Histogram definition
  //h_weights = new TH1F("h_weights","h_weights",1000,-10,10);

  t_parton_pt = new TH1F("t_parton_pt","t_parton_pt",120,0.,1200); 
  //t_parton_px = new TH1F("t_parton_px","t_parton_px",200,-1600,1600);
  //t_parton_py = new TH1F("t_parton_py","t_parton_py",200,-1600,1600);
  //t_parton_pz = new TH1F("t_parton_pz","t_parton_pz",200,-2600,2600);
  t_parton_m  = new TH1F("t_parton_m","t_parton_m",2000,10,1000);

  b_parton_pt = new TH1F("b_parton_pt","b_parton_pt",100,0.,400); 
  //b_parton_px = new TH1F("b_parton_px","b_parton_px",200,-1000,1000);
  //b_parton_py = new TH1F("b_parton_py","b_parton_py",200,-800,800);
  //b_parton_pz = new TH1F("b_parton_pz","b_parton_pz",200,-2000,2000);
  b_parton_m  = new TH1F("b_parton_m","b_parton_m",200,4.65,4.85);

  th_parton_pt = new TH1F("th_parton_pt","th_parton_pt",150,0.,1500); 
  //th_parton_px = new TH1F("th_parton_px","th_parton_px",200,-2600,2600);
  //th_parton_py = new TH1F("th_parton_py","th_parton_py",200,-2600,2600);
  //th_parton_pz = new TH1F("th_parton_pz","th_parton_pz",200,-4000,4000);
  th_parton_m  = new TH1F("th_parton_m","th_parton_m",100,165,180);

  bh_parton_pt = new TH1F("bh_parton_pt","bh_parton_pt",150,0.,1500); 
  //bh_parton_px = new TH1F("bh_parton_px","bh_parton_px",300,-4000,4000);
  //bh_parton_py = new TH1F("bh_parton_py","bh_parton_py",300,-4000,4000);
  //bh_parton_pz = new TH1F("bh_parton_pz","bh_parton_pz",300,-4000,4000);
  bh_parton_m  = new TH1F("bh_parton_m","bh_parton_m",200,4.7,4.9);

  h_parton_pt = new TH1F("h_parton_pt","h_parton_pt",150,0.,1000); 
  //h_parton_px = new TH1F("h_parton_px","h_parton_px",200,-1400,1400);
  //h_parton_py = new TH1F("h_parton_py","h_parton_py",200,-1400,1400);
  //h_parton_pz = new TH1F("h_parton_pz","h_parton_pz",200,-4000,4000);
  h_parton_m  = new TH1F("h_parton_m","h_parton_m",400,950,1050);

  tb_system_m = new TH1F("tb_system_m","tb_system_m",100,170,180);
  tb_system_e = new TH1F("tb_system_e","tb_system_e",200,0,4000);

  thbh_system_m = new TH1F("thbh_system_m","thbh_system_m",100,10,400);
  thbh_system_e = new TH1F("thbh_system_e","thbh_system_e",100,900,2000);

  tbh_system_m = new TH1F("tbh_system_m","tbh_system_m",100,1150,1200);
  tbh_system_e = new TH1F("tbh_system_e","tbh_system_e",300,1000,5000);

  tb_system_dR = new TH1F("tb_system_dR","tb_system_dR",100,0,12);
  thbh_system_dR = new TH1F("thbh_system_dR","thbh_system_dR",100,0,9);
  
  extraJ_pt = new TH1F("extraJ_pt","extraJ_pt",150,0,800);
  extraJ_eta = new TH1F("extraJ_eta","extraJ_eta",100,-10,10);

  extraJ_deta_top = new TH1F("extraJ_deta_top","extraJ_deta_top",100,-5,5);
  extraJ_dphi_top = new TH1F("extraJ_dphi_top","extraJ_dphi_top",100,-10,10);
  extraJ_deta_b = new TH1F("extraJ_deta_b","extraJ_deta_b",100,-10,10);
  extraJ_dphi_b = new TH1F("extraJ_dphi_b","extraJ_dphi_b",100,-10,10);
  extraJ_deta_th = new TH1F("extraJ_deta_th","extraJ_deta_th",100,-8,8);
  extraJ_dphi_th = new TH1F("extraJ_dphi_th","extraJ_dphi_th",100,-10,10);
  extraJ_deta_bh = new TH1F("extraJ_deta_bh","extraJ_deta_bh",100,-8,8);
  extraJ_dphi_bh = new TH1F("extraJ_dphi_bh","extraJ_dphi_bh",100,-10,10);
  extraJ_deta_h = new TH1F("extraJ_deta_h","extraJ_deta_h",100,-8,8);
  extraJ_dphi_h = new TH1F("extraJ_dphi_h","extraJ_dphi_h",100,-10,10);

  //Histogram registration

  //CHECK(histSvc->regHist("/MYSTREAM/h_weights",h_weights));
  //CHECK(histSvc->regHist("/MYSTREAM/t_parton_pt",t_parton_pt));
  //CHECK(histSvc->regHist("/MYSTREAM/t_parton_px",t_parton_px));
  //CHECK(histSvc->regHist("/MYSTREAM/t_parton_py",t_parton_py));
  //CHECK(histSvc->regHist("/MYSTREAM/t_parton_pz",t_parton_pz));
  CHECK(histSvc->regHist("/MYSTREAM/t_parton_m",t_parton_m));
            
  CHECK(histSvc->regHist("/MYSTREAM/b_parton_pt",b_parton_pt));
  //CHECK(histSvc->regHist("/MYSTREAM/b_parton_px",b_parton_px));
  //CHECK(histSvc->regHist("/MYSTREAM/b_parton_py",b_parton_py));
  //CHECK(histSvc->regHist("/MYSTREAM/b_parton_pz",b_parton_pz));
  CHECK(histSvc->regHist("/MYSTREAM/b_parton_m",b_parton_m));
             
  CHECK(histSvc->regHist("/MYSTREAM/th_parton_pt",th_parton_pt));
  //CHECK(histSvc->regHist("/MYSTREAM/th_parton_px",th_parton_px));
  //CHECK(histSvc->regHist("/MYSTREAM/th_parton_py",th_parton_py));
  //CHECK(histSvc->regHist("/MYSTREAM/th_parton_pz",th_parton_pz));
  CHECK(histSvc->regHist("/MYSTREAM/th_parton_m",th_parton_m));
             
  CHECK(histSvc->regHist("/MYSTREAM/bh_parton_pt",bh_parton_pt));
  //CHECK(histSvc->regHist("/MYSTREAM/bh_parton_px",bh_parton_px));
  //CHECK(histSvc->regHist("/MYSTREAM/bh_parton_py",bh_parton_py));
  //CHECK(histSvc->regHist("/MYSTREAM/bh_parton_pz",bh_parton_pz));
  CHECK(histSvc->regHist("/MYSTREAM/bh_parton_m",bh_parton_m));
             
  CHECK(histSvc->regHist("/MYSTREAM/h_parton_pt",h_parton_pt));
  //CHECK(histSvc->regHist("/MYSTREAM/h_parton_px",h_parton_px));
  //CHECK(histSvc->regHist("/MYSTREAM/h_parton_py",h_parton_py));
  //CHECK(histSvc->regHist("/MYSTREAM/h_parton_pz",h_parton_pz));
  CHECK(histSvc->regHist("/MYSTREAM/h_parton_m",h_parton_m));

  CHECK(histSvc->regHist("/MYSTREAM/tb_system_m",tb_system_m));
  CHECK(histSvc->regHist("/MYSTREAM/tb_system_e",tb_system_e));

  CHECK(histSvc->regHist("/MYSTREAM/thbh_system_m",thbh_system_m));
  CHECK(histSvc->regHist("/MYSTREAM/thbh_system_e",thbh_system_e));

  CHECK(histSvc->regHist("/MYSTREAM/tbh_system_m",tbh_system_m));
  CHECK(histSvc->regHist("/MYSTREAM/tbh_system_e",tbh_system_e));

  CHECK(histSvc->regHist("/MYSTREAM/tb_system_dR",tb_system_dR));
  CHECK(histSvc->regHist("/MYSTREAM/thbh_system_dR",thbh_system_dR));

  CHECK(histSvc->regHist("/MYSTREAM/extraJ_pt",extraJ_pt));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_eta",extraJ_eta));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_deta_top",extraJ_deta_top));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_dphi_top",extraJ_dphi_top));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_deta_b",extraJ_deta_b));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_dphi_b",extraJ_dphi_b));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_deta_th",extraJ_deta_th));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_dphi_th",extraJ_dphi_th));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_deta_bh",extraJ_deta_bh));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_dphi_bh",extraJ_dphi_bh));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_deta_h",extraJ_deta_h));
  CHECK(histSvc->regHist("/MYSTREAM/extraJ_dphi_h",extraJ_dphi_h));

  return StatusCode::SUCCESS;
}

StatusCode TruthAnalysisAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //
  ATH_MSG_INFO("sumOfWeights is " << sumOfWeights);


  return StatusCode::SUCCESS;
}

StatusCode TruthAnalysisAlg::execute() {  

  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  // Assorted variablrs
  float GeV = 1000.;
  float weight = 1.;
  unsigned int nTop=0;
  unsigned int nBWp=0;
  unsigned int nBTop=0;
  unsigned int nWx=0;


  // EventInfo for the weight

  const xAOD::EventInfo *eventInfo = nullptr;
  CHECK(evtStore()->retrieve(eventInfo,"EventInfo"));
  //for(auto weightName : m_truthWeightTool->getWeightNames()){
  //  ATH_MSG_INFO("Weight: " << weightName);
  //}
  //weight = m_truthWeightTool->getWeight(m_weightName);
  weight = eventInfo->mcEventWeights().at(0);
  //weight =1.0;

  sumOfWeights+=weight;

  const xAOD::TruthParticle* top_from_H = nullptr;
  const xAOD::TruthParticle* bot_from_H = nullptr;
  const xAOD::TruthParticle* cH = nullptr;
  const xAOD::TruthParticle* top = nullptr;
  const xAOD::TruthParticle* bot = nullptr;
  const xAOD::TruthParticle* extra_radiation = nullptr;

  //// START SELECTION OF everything
  const xAOD::TruthParticleContainer* truthParticles = nullptr;
  CHECK(evtStore()->retrieve(truthParticles,"TruthParticles"));
  auto iterPart = truthParticles->begin();
  //Main TruthParticle loop
  for(; iterPart!=truthParticles->end(); ++iterPart){
    const xAOD::TruthParticle* particle = (*iterPart);
      //If it comes from H+
      if(particle->nParents()==1 
        && particle->parent(0)->absPdgId()==37){
        //Store if it is top or b
        if(particle->isTop()){top_from_H = particle; ++nTop;}
        if(particle->absPdgId()==5){bot_from_H = particle; ++nBWp;}
      }//End if it comes from H+
      //if(particle->nParents()==1
      //  && particle->parent(0)->pdgId()==-37){
        //h_weights->Fill(-5);
      //}
      //if(particle->nParents()==1
      //  && particle->parent(0)->pdgId()==37){
        //h_weights->Fill(5);
      //}
      //If it comes directly from the incoming particles
      if(particle->nParents()==2 
        && particle->parent(0)->isParton() && particle->parent(0)->status()==21
        && particle->parent(1)->isParton() && particle->parent(1)->status()==21){
        //Store if it is top or b
        if(particle->isTop()){top = particle; ++nTop;}
        if(particle->absPdgId()==5){bot = particle; ++nBWp;}
        if(particle->absPdgId()==37) cH = particle;
        //Obtain extra radiation particle with > 20GeV pt
        ATH_MSG_DEBUG ("Getting to extra radiation");
        if(particle->parent(0)->nChildren()>3){
          ATH_MSG_DEBUG ("Has extra radiation");
          if(particle->absPdgId()!=5 && 
             particle->absPdgId()!=37 &&
             particle->pt()/GeV > 20){
              ATH_MSG_DEBUG ("Is extra radiation and has pt>20GeV");
              extra_radiation = particle;
          }
        }
      }//End if it comes from the incoming
  }//End particle loop

 ///Find the last version of top, the W and the bTop 
//const xAOD::TruthParticle* bTop = nullptr;
//const xAOD::TruthParticle* W = nullptr;
//const xAOD::TruthParticle* lastTop = getLastVersion(top);
//if(lastTop->child(0)->absPdgId()==5 && lastTop->child(1)->absPdgId()==24){ bTop=lastTop->child(0);W=lastTop->child(1);}
//else if(lastTop->child(1)->absPdgId()==5 && lastTop->child(0)->absPdgId()==24){ bTop=lastTop->child(1);W=lastTop->child(0);}
//else{ATH_MSG_ERROR("Weird top decay");return StatusCode::FAILURE;}

//if(nTop==1 && nBWp==1){
//  setFilterPassed(true); //if got here, assume that means algorithm passed
//}else{
//  return StatusCode::FAILURE;
//}

//Find the last version of W and the W daughters
//const xAOD::TruthParticle* lastW = getLastVersion(W);
//const xAOD::TruthParticle* w1 = lastW->child(0);
//const xAOD::TruthParticle* w2 = lastW->child(1);
ATH_MSG_DEBUG ("Setting Lorentz vectors");
//Convert to LorentVectors
TLorentzVector l_t,l_b,l_tH,l_bH,l_cH,l_xR;
l_t.SetPxPyPzE(top->px(),top->py(),top->pz(),top->e());
l_b.SetPxPyPzE(bot->px(),bot->py(),bot->pz(),bot->e());
l_tH.SetPxPyPzE(top_from_H->px(),top_from_H->py(),top_from_H->pz(),top_from_H->e());
l_bH.SetPxPyPzE(bot_from_H->px(),bot_from_H->py(),bot_from_H->pz(),bot_from_H->e());
l_cH.SetPxPyPzE(cH->px(),cH->py(),cH->pz(),cH->e());
if(extra_radiation!=nullptr){
  l_xR.SetPxPyPzE(extra_radiation->px(),extra_radiation->py(),extra_radiation->pz(),extra_radiation->e());
}
//l_lep.SetPxPyPzE(w1->px(),w1->py(),w1->pz(),w1->e());
//l_nu.SetPxPyPzE(w2->px(),w2->py(),w2->pz(),w2->e());
//if(w2->absPdgId()==11 || w2->absPdgId()==13 || w2->absPdgId()==15){
//  l_lep.SetPxPyPzE(w2->px(),w2->py(),w2->pz(),w2->e());
//  l_nu.SetPxPyPzE(w1->px(),w1->py(),w1->pz(),w1->e());
//}



//Composites
//TLorentzVector l_tb,l_bbw1w2,l_w1w2,l_bw1w2;
//l_tb = l_bWp + l_t;
//l_bbw1w2 = l_bWp + l_bTop + l_w1 + l_w2;
//l_w1w2 = l_w1 + l_w2;
//l_bw1w2 = l_bTop + l_w1 + l_w2;

//float parton_deltaR = Func::DistEtaPhi(l_t.Rapidity(),l_t.Phi(),l_bWp.Rapidity(),l_bWp.Phi());
//float top_distance1 = Func::DistEtaPhi(l_w1.Rapidity(),l_w1.Phi(),l_w2.Rapidity(),l_w2.Phi());
//float top_distance2 = Func::DistEtaPhi(l_w1.Rapidity(),l_w1.Phi(),l_bTop.Rapidity(),l_bTop.Phi());
//float top_distance3 = Func::DistEtaPhi(l_w2.Rapidity(),l_w2.Phi(),l_bTop.Rapidity(),l_bTop.Phi());
//float avg_distance = top_distance1+top_distance2+top_distance3;
//avg_distance = avg_distance/3.;

//TLorentzVector l_jtop,l_jbWp;
//float dist_j_top = 99;
//float dist_j_b = 99;
/*
//Loop over truth small-R jets
const xAOD::JetContainer* truthJetsContainer = 0;
std::vector<TLorentzVector> v_truthJets;
CHECK(evtStore()->retrieve(truthJetsContainer, "AntiKt4TruthDressedWZJets"));
for (auto truthJet : *truthJetsContainer){
  TLorentzVector l_jet;
  l_jet.SetPxPyPzE(truthJet->px(),truthJet->py(),truthJet->pz(),truthJet->e());
  float dist = Func::DistEtaPhi(l_jet.Rapidity(),l_jet.Phi(),l_bWp.Rapidity(),l_bWp.Phi());
  if (dist < dist_j_b){
    dist_j_b = dist;
    l_jbWp = l_jet;
  }
}

//Loop over truth small-R jets
const xAOD::JetContainer* truthLJetsContainer = 0;
std::vector<TLorentzVector> v_truthLJets;
CHECK(evtStore()->retrieve(truthLJetsContainer, "AntiKt10TruthTrimmedPtFrac5SmallR20Jets"));
for (auto truthLJet : *truthLJetsContainer){
  TLorentzVector l_ljet;
  l_ljet.SetPxPyPzE(truthLJet->px(),truthLJet->py(),truthLJet->pz(),truthLJet->e());
  float dist = Func::DistEtaPhi(l_ljet.Rapidity(),l_ljet.Phi(),l_t.Rapidity(),l_t.Phi());
  if (dist < dist_j_top){
    dist_j_top = dist;
    l_jtop = l_ljet;
  }
}

//Composites
TLorentzVector l_had_tb;
l_had_tb = l_jbWp + l_jtop;
float hadron_deltaR = Func::DistEtaPhi(l_jtop.Rapidity(),l_jtop.Phi(),l_jbWp.Rapidity(),l_jbWp.Phi());

//Containment fraction
float w1_dist = Func::DistEtaPhi(l_jtop.Rapidity(),l_jtop.Phi(),l_w1.Rapidity(),l_w1.Rapidity());
float w2_dist = Func::DistEtaPhi(l_jtop.Rapidity(),l_jtop.Phi(),l_w2.Rapidity(),l_w2.Rapidity());
float bTop_dist = Func::DistEtaPhi(l_jtop.Rapidity(),l_jtop.Phi(),l_bTop.Rapidity(),l_bTop.Rapidity());

int isContained = 0;
if(w1_dist <=1.0 && w2_dist <=1.0 && bTop_dist < 1.0) isContained=1;
*/
  ATH_MSG_DEBUG ("Filling Histograms");
  //Fill histograms
  //h_weights->Fill(weight);
  t_parton_pt->Fill(l_t.Pt()/GeV,weight);
  //t_parton_px->Fill(l_t.Px()/GeV,weight);
  //t_parton_py->Fill(l_t.Py()/GeV,weight);
  //t_parton_pz->Fill(l_t.Pz()/GeV,weight);
  t_parton_m->Fill(l_t.M()/GeV,weight);
              
  b_parton_pt->Fill(l_b.Pt()/GeV,weight);
  //b_parton_px->Fill(l_b.Px()/GeV,weight);
  //b_parton_py->Fill(l_b.Py()/GeV,weight);
  //b_parton_pz->Fill(l_b.Pz()/GeV,weight);
  b_parton_m->Fill(l_b.M()/GeV,weight);
              
  th_parton_pt->Fill(l_tH.Pt()/GeV,weight);
  //th_parton_px->Fill(l_tH.Px()/GeV,weight);
  //th_parton_py->Fill(l_tH.Py()/GeV,weight);
  //th_parton_pz->Fill(l_tH.Pz()/GeV,weight);
  th_parton_m->Fill(l_tH.M()/GeV,weight);             

  bh_parton_pt->Fill(l_bH.Pt()/GeV,weight);
  //bh_parton_px->Fill(l_bH.Px()/GeV,weight);
  //bh_parton_py->Fill(l_bH.Py()/GeV,weight);
  //bh_parton_pz->Fill(l_bH.Pz()/GeV,weight);
  bh_parton_m->Fill(l_bH.M()/GeV,weight);

  h_parton_pt->Fill(l_cH.Pt()/GeV,weight);
  //h_parton_px->Fill(l_cH.Px()/GeV,weight);
  //h_parton_py->Fill(l_cH.Py()/GeV,weight);
  //h_parton_pz->Fill(l_cH.Pz()/GeV,weight);
  h_parton_m->Fill(l_cH.M()/GeV,weight);

  tb_system_m->Fill( l_t.M()/GeV + l_b.M()/GeV , weight );
  tb_system_e->Fill( l_t.E()/GeV + l_b.E()/GeV , weight );

  thbh_system_m->Fill( l_tH.M()/GeV + l_bH.M()/GeV , weight );
  thbh_system_e->Fill( l_tH.E()/GeV + l_bH.E()/GeV , weight );

  tbh_system_m->Fill( l_t.M()/GeV + l_b.M()/GeV + l_cH.M()/GeV , weight );
  tbh_system_e->Fill(l_t.E()/GeV + l_b.E()/GeV + l_cH.E()/GeV , weight );

  tb_system_dR->Fill( Func::DistEtaPhi(l_t.PseudoRapidity(),l_t.Phi(),l_b.PseudoRapidity(),l_b.PseudoRapidity()) , weight);
  thbh_system_dR->Fill( Func::DistEtaPhi(l_tH.PseudoRapidity(),l_tH.Phi(),l_bH.PseudoRapidity(),l_bH.PseudoRapidity()) , weight );

  if(extra_radiation!=nullptr){
    extraJ_pt->Fill(l_xR.Pt()/GeV);
    extraJ_eta->Fill(l_xR.PseudoRapidity());

    extraJ_deta_top->Fill(l_xR.PseudoRapidity() - l_t.PseudoRapidity());
    extraJ_dphi_top->Fill(l_xR.Phi() - l_t.Phi());
    extraJ_deta_b->Fill(l_xR.PseudoRapidity() - l_b.PseudoRapidity());
    extraJ_dphi_b->Fill(l_xR.Phi() - l_b.Phi());
    extraJ_deta_th->Fill(l_xR.PseudoRapidity() - l_tH.PseudoRapidity());
    extraJ_dphi_th->Fill(l_xR.Phi() - l_tH.Phi());
    extraJ_deta_bh->Fill(l_xR.PseudoRapidity() - l_bH.PseudoRapidity());
    extraJ_dphi_bh->Fill(l_xR.Phi() - l_bH.Phi());
    extraJ_deta_h->Fill(l_xR.PseudoRapidity() - l_cH.PseudoRapidity());
    extraJ_dphi_h->Fill(l_xR.Phi() - l_cH.Phi());
  }

  return StatusCode::SUCCESS;
}

StatusCode TruthAnalysisAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}

const xAOD::TruthParticle* TruthAnalysisAlg::getLastVersion(const xAOD::TruthParticle* particle){

  const xAOD::TruthParticle* result = particle;

  //Recursively call till we get no children with teh same name
  for(int i = 0; i < result->nChildren(); i++){
    const xAOD::TruthParticle* next = result->child(i);
    if(next->absPdgId() == result->absPdgId()){
      result = getLastVersion(next);
    }
  }
  return result;
}




