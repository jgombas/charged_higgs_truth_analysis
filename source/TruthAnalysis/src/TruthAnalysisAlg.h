#ifndef TBTRUTHANALYSIS_TRUTHANALYSISALG_H
#define TBTRUTHANALYSIS_TRUTHANALYSISALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "xAODTruth/TruthParticle.h"
#include "PMGTools/PMGTruthWeightTool.h"

//Example ROOT Includes
//#include "TTree.h"
#include "TH1D.h"



class TruthAnalysisAlg: public ::AthAnalysisAlgorithm { 
 public: 
  TruthAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~TruthAnalysisAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  virtual StatusCode  execute();        //per event
  virtual StatusCode  finalize();       //once, after all events processed
  const xAOD::TruthParticle* getLastVersion(const xAOD::TruthParticle* particle);
  


 private: 
  
  std::string m_weightName;

  ToolHandle<PMGTools::IPMGTruthWeightTool> m_truthWeightTool;

  //TH1F* h_weights = nullptr;

  TH1F* t_parton_pt = nullptr;
  //TH1F* t_parton_px = nullptr;
  //TH1F* t_parton_py = nullptr;
  //TH1F* t_parton_pz = nullptr;
  TH1F* t_parton_m  = nullptr;

  TH1F* b_parton_pt = nullptr;
  //TH1F* b_parton_px = nullptr;
  //TH1F* b_parton_py = nullptr;
  //TH1F* b_parton_pz = nullptr;
  TH1F* b_parton_m  = nullptr;

  TH1F* th_parton_pt = nullptr;
  //TH1F* th_parton_px = nullptr;
  //TH1F* th_parton_py = nullptr;
  //TH1F* th_parton_pz = nullptr;
  TH1F* th_parton_m  = nullptr;

  TH1F* bh_parton_pt = nullptr;
  //TH1F* bh_parton_px = nullptr;
  //TH1F* bh_parton_py = nullptr;
  //TH1F* bh_parton_pz = nullptr;
  TH1F* bh_parton_m  = nullptr;

  TH1F* h_parton_pt = nullptr;
  //TH1F* h_parton_px = nullptr; 
  //TH1F* h_parton_py = nullptr;
  //TH1F* h_parton_pz = nullptr;
  TH1F* h_parton_m  = nullptr;

  TH1F* tb_system_m = nullptr;
  TH1F* tb_system_e = nullptr;

  TH1F* thbh_system_m = nullptr;
  TH1F* thbh_system_e = nullptr;

  TH1F* tbh_system_m = nullptr;
  TH1F* tbh_system_e = nullptr;

  TH1F* tb_system_dR = nullptr;
  TH1F* thbh_system_dR = nullptr;

  TH1F* extraJ_pt = nullptr;
  TH1F* extraJ_eta = nullptr;

  TH1F* extraJ_deta_top = nullptr;
  TH1F* extraJ_dphi_top = nullptr;
  TH1F* extraJ_deta_b = nullptr;
  TH1F* extraJ_dphi_b = nullptr;
  TH1F* extraJ_deta_th = nullptr;
  TH1F* extraJ_dphi_th = nullptr;
  TH1F* extraJ_deta_bh = nullptr;
  TH1F* extraJ_dphi_bh = nullptr;
  TH1F* extraJ_deta_h = nullptr;
  TH1F* extraJ_dphi_h = nullptr;

  float sumOfWeights;


}; 

#endif //> !TRUTHANALYSIS_TRUTHANALYSISALG_H
