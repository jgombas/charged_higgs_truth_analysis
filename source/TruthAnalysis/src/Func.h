#ifndef Func_H
#define Func_H
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Class to store all the functions I'm calling hundreds and hundreds of times from the other classes/////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "TMath.h"
#include "TLorentzVector.h"
#include <iostream>

class Func{

	public:
		Func();
		~Func();
		static double Angle(TLorentzVector v1, TLorentzVector v2);
		static double DistEtaPhi(double eta1,double phi1, double eta2, double phi2);
		static double DeltaPhi(double phi1,double phi2);
		static int findMuspContainer(std::vector<float> *musp_eta, std::vector<float> *musp_phi,float jet_eta, float jet_phi);
		static int samplingNumber(std::string detName); 
																						      
	private:

		
};

#endif // Func_H

